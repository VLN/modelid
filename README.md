# <u>Modèle d'identité</u> : Guide de déploiement et d'utilisation

Ce modèle d'identité à été réalisé dans le cadre d'un projet scolaire. Il s'agit d'une sous-partie d'une majeure consacrée au renseignement et à l'anonymat.

## Modélisation

Le fichier de modélisation de la base de données du modèle d'identité est disponible dans `./Modélisation`.

Il est nécessaire d'installer `SQL Power Architect` afin de pouvoir le visualiser. 

## MySQL

### Installation de MySQL

Si vous disposez déjà d'une version de `MySQL` sur votre ordinateur, vous pouvez sauter cette partie.

#### Arch Linux & Manjaro

Sur *ArchLinux* et les distributions basées sur celle-ci, on va installer le fork de `MySQL`, `mariadb` via le terminal. Les commandes ci-dessous décrivent cette installation ainsi que les pré-requis de configuration.

```bash
# Mise-à-jour des paquets
$ sudo pacman -Syu
# Installation de mariadb via le gestionnaire de paquets pacman
$ sudo pacman -S mariadb
# Si les tables ne sont pas mises en place automatiquement (erreur lors du démarrage du service), installez-les
$ mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
# Démarrage du service
$ sudo systemctl start mariadb
# Sécurisation de mariadb
$ sudo mysql_secure_installation
```

##### `mysql_secure_installation` : erreur

A cause des plugins d'authentification utilisés par `mariadb` (et par `MySQL` de façon générale), il est possible qu'une erreur remonte lors de l'action de sécurisation de `MySQL` : `ERROR 1698 (28000): Access denied for user 'root'@'localhost'`. Ci-dessous, vous trouverez les actions permettant de résoudre cet incident :

```bash
# Connexion à MySQL
$ sudo mariadb -u root
# Vérification des plugins d'authentification
mysql> USE mysql;
mysql> SELECT User, Host, plugin FROM mysql.user;
+-------+-----------+-----------------------+
| User  | Host      | plugin                |
+-------+-----------+-----------------------+
| root  | localhost | auth_socket           |
| mysql | localhost | mysql_native_password |
+-------+-----------+-----------------------+
# Changement du plugin pour root
mysql> UPDATE user SET plugin='mysql_native_password' WHERE User='root';
mysql> FLUSH PRIVILEGES;
mysql> exit;
# Redémarrage du service
$ systemctl mariadb restart
# Relancer la sécurisation de mariadb
$ sudo mysql_secure_installation
```

#### Debian & Ubuntu

Sur Debian et ses distributions dérivées, on va installer `MySQL` et configurer quelques pré-requis via les commandes suivantes :

```bash
# Mise-à-jour des paquets
$ sudo apt-get update
# Installation de MySQL depuis le gestionnaire de paquets aptitute
$ sudo apt install mysql-server
# Démarrage du service
$ sudo systemctl start mysql
# Sécurisation de MySQL
$ sudo mysql_secure_installation
```

### Création de la base de données ModelId

#### Base de données en clair

Une fois `MySQL` installé, il faut créer la base de données — *ModelId* — qui contiendra le modèle d'identité. Pour cela, on pourra utiliser le script `Scripts_SQL/script creation_ModelId.sql` en le lançant directement depuis `MySQL` : 

```mysql
mysql> source filepath/creation_ModelId.sql
```

Il est également possible de lancer le script directement depuis le terminal : 

```bash
$ mysql -u root -p < filepath/creation_ModelId.sql
```

#### Base de données chiffrée

##### Clefs de chiffrement

Pour des raisons de sécurité — et notamment en fonction de l'usage que vous ferez de la base de données *ModelId* — il peut-être nécessaire de chiffrer la base de données, ou tout du moins les informations contenues dans celle-ci.

On va utiliser `openssl` pour générer une *clef* de chiffrement ; et l'utiliser par la suite pour chiffrer les tables de *ModelId* :

```bash
# Génération de la clef de chiffrement
$ openssl rand -hex 16
b981242dd8265caa488b35046aad445b
```

 On crée un fichier (`keys_Mariadb.txt`) et on y glisse l'identifiant de la clef (1 dans cet exemple, le fichier pouvant contenir différentes clefs utilisées pour chiffrer différentes bases de données) ainsi que la clef générée précédemment :

```
1;b981242dd8265caa488b35046aad445b
```

Pour davantage de sécurité, il est conseillé de chiffrer également cette clef de chiffrement ([documentation officielle](https://mariadb.com/kb/en/library/file-key-management-encryption-plugin/#installing-the-plugin)).

##### Plugin

Le chiffrement se fait à l'aide d'un plugin `MySQL` :  `file_key_management`. Sous `mariadb`, il est disponible à partir de la version 10.1.3. 

```bash
# Copie du fichier de configuration
$ sudo cp /etc/my.cnf /etc/my.cnf.backup
```

On va ensuite ajouter les lignes suivantes dans le fichier de configuration :

```bash
# Encryption
file_key_management_filename = /etc/my.cnf.d/keys_Mariadb.txt
```

Il ne reste plus qu'à installer le plugin : 

```bash
# Redémarrage du service
$ sudo systemctl restart mariadb
# Installation du plugin
$ mysql -u root -p
mysql> install soname 'file_key_management'
```

Il ne reste plus qu'à tester le bon fonctionnement du plugin. Pour cela, on va créer une table — dans une base de données quelconque — et vérifier que cela fonctionne bien. En créant la table, il suffit d'ajouter `ENCRYPTED=YES ENCRYPTION_KEY_ID=1`. 

##### Génération de la base de données *ModelId*

Une fois plugin installé, on va générer la base de données contenant le modèle d'identité, *ModelId*. Pour cela, on va utiliser le script `Scripts_SQL/creation_ModelId_chiff.sql` ; il est possible de le faire depuis le terminal ou bien directement depuis l'invite de commande de `mariadb`.

```bash
# Génération depuis le terminal
$ mysql -u root -p < filepath/creation_ModelId.sql
# Génération depuis l'invite de commande mariadb
mysql> source filepath/creation_ModelId_chiff.sql
```

#### Création d'un utilisateur spécifique pour *ModelId*

Pour des raisons de sécurité et de façon générale, il est préférable de ne pas se connecter en tant qu'utilisateur `root` pour manager une base de données. Une fois la base de données générée, il est fortement conseillé de créer un utilisateur pour gérer la base *ModelId* afin de ne pas utiliser le compte `root` pour le faire. 

Les commandes ci-dessous permettent de créer un utilisateur pour la base *ModelId* :

```mysql
-- Création d'un utilisateur user_MI (pensez à remplacer password)
mysql> CREATE USER 'user_ModelId'@'localhost' IDENTIFIED BY 'password';
-- Attribution des droits sur la base de données ModelId
mysql> GRANT ALL PRIVILEGES ON ModelId TO 'user_ModelId'@localhost;
mysql> FLUSH PRIVILEGES;
```

On peut également créer des utilisateurs ayant des droits restreints, en lecture seule par exemple pour des applications de visualisation (grafana, etc.).

### Données de test

Pour effectuer des premières visualisations et requêtes afin de manipuler l'outil et si vous ne disposez pas de données, vous pouvez utiliser le script `Scripts_SQL/test.sql` pour remplir la base de données *ModelId* : 

```bash
# Depuis un terminal linux
mysql> source filepath/test.sql
# Depuis l'invite de commande MySQL
$ mysql -u user_MI -p < filepath/test.sql
```

## Interface de visualisation

Comme interface de visualisation, on va utiliser l'outil open source `grafana`. Il s'installe simplement depuis son gestionnaire de paquets : 

```bash
$ sudo pacman -S grafana
```

Si ce n'est pas déjà fait, on va créer un utilisateur disposant de droits uniquement en lecture sur la base de données *ModelId* ; en effet, `grafana` n'effectue que de la visualisation.

```mysql
-- Création de l'utilsateur (pensez à remplacer password)
mysql> CREATE USER 'user_ModelId_RO'@'localhost' IDENTIFIED BY 'password';
-- Attribution du droit de lecture à l'utilisateur
mysql> GRANT SELECT ON ModelId.* TO 'user_ModelId_RO'@'localhost';
```

On démarre ensuite `grafana` via `sudo sytemctl start grafana` ; il suffit ensuite de se connecter à `127.0.0.1:3000` pour se rendre sur l'interface web de l'outil. Puis, on crée une nouvelle source de données pour une entrée de type `MySQL` : 

```
Name : ModelId
Host : localhost:3306
Database : ModelId
User : user_ModelId_RO
Password : ***********
```

Pour terminer, vous pouvez créer vos dashboards afin de visualiser ce qui vous semble pertinent.

#### Installation de plugins

Une librairie de plugins pour `Grafana` est disponible ; elle permet notamment de disposer d'autres types de représentation comme par exemple.

Par exemple, pour la table Localisation, on va installer un plugin qui gère les cartes :

```bash
# Chercher les plugins de type map
$ grafana-cli plugins list-remote | grep map
id: alexandra-trackmap-panel version: 1.2.0
id: flant-statusmap-panel version: 0.1.1
id: grafana-worldmap-panel version: 0.2.1
id: mtanda-heatmap-epoch-panel version: 0.1.7
id: neocat-cal-heatmap-panel version: 0.0.3
id: pr0ps-trackmap-panel version: 2.0.4
id: savantly-heatmap-panel version: 0.2.0
# Installation du plugin
$ sudo grafana-cli plugins install alexandra-trackmap-panel
```

#### Import d'un dashboard

Grafana permet d'exporter et importer des dashboards au format JSON. Un exemple de dashboard pour la base de données *ModelId* est disponible sous `Grafana/ModelId-1578602298421.json`.

Il suffit ensuite de créer un nouveau dashboard et d'importer le fichier JSON (https://grafana.com/docs/grafana/latest/reference/export_import/).

### TODO

* script python pour remplir les tables automatiquement (via un format de données JSON)
* tester grafana avec le chiffrement
* table(s) pour modéliser le comportement & caractère