# Modèle identité

## Sources

Attributs : 

* https://www.fastcompany.com/90310803/here-are-the-data-brokers-quietly-buying-and-selling-your-personal-information (génral, idées de prêt, vérifications des informations déjà sur le modèle)
* https://www.caisse-epargne.fr/particuliers/emprunter (emprunts)
* Personal Data Trading Scheme for Data Brokers in IoT Data Marketplaces (pdf google)
* https://www.ftc.gov/system/files/documents/reports/data-brokers-call-transparency-accountability-report-federal-trade-commission-may-2014/140527databrokerreport.pdf
* [https://www.biometricsinstitute.org/what-is-biometrics/types-of-biometrics/](https://slack-redir.net/link?url=https%3A%2F%2Fwww.biometricsinstitute.org%2Fwhat-is-biometrics%2Ftypes-of-biometrics%2F&v=3) => biométrie
* https://www.ft.com/content/f1590694-fe68-11e8-aebf-99e208d3e521 (addictions, etc.)

