-- Création de la base de données
CREATE DATABASE ModelId;

-- Séléction de la base de données
USE ModelId;

-- Création des tables
CREATE TABLE Club (
  idCL INT NOT NULL,
  Nom VARCHAR(50),
  Ville VARCHAR(50),
  DateDebut DATE,
  DateFin DATE,
  PRIMARY KEY (idCL)
) ENGINE InnoDB;


CREATE TABLE Contrat (
  idCN INT NOT NULL,
  idFA INT NOT NULL,
  Type VARCHAR(50),
  Services VARCHAR(100),
  Reference VARCHAR(50),
  Tarif FLOAT,
  Numero VARCHAR(50),
  Divers VARCHAR(150),
  DateDebut DATE,
  DateFin DATE,
  PRIMARY KEY (idCN)
) ENGINE InnoDB;


CREATE TABLE FAI (
  idFA INT NOT NULL,
  Nom VARCHAR(50),
  PRIMARY KEY (idFA)
) ENGINE InnoDB;


CREATE TABLE HistoAchats (
  idHA INT NOT NULL,
  Moyen VARCHAR(50),
  Prix FLOAT,
  Vendeur VARCHAR(50),
  Type VARCHAR(50),
  Ville VARCHAR(50),
  Nom VARCHAR(50),
  Reference VARCHAR(50),
  Marque VARCHAR(50),
  Date DATE,
  PRIMARY KEY (idHA)
) ENGINE InnoDB;


CREATE TABLE Emprunt (
  idEM INT NOT NULL,
  Montant FLOAT,
  TauxInteret FLOAT,
  TauxAssurance FLOAT,
  Duree FLOAT,
  DateDebut DATE,
  DateFin DATE,
  PRIMARY KEY (idEM)
) ENGINE InnoDB;


CREATE TABLE Certification (
  idCE INT NOT NULL,
  Nom VARCHAR(50),
  Domaine VARCHAR(50),
  Organisme VARCHAR(50),
  Note VARCHAR(50),
  Reference VARCHAR(50),
  DateDebut DATE,
  DateFin DATE,
  PRIMARY KEY (idCE)
) ENGINE InnoDB;


CREATE TABLE Organisation (
  idOR INT NOT NULL,
  Nom VARCHAR(50),
  Type VARCHAR(50),
  SIREN INT,
  Secteur VARCHAR(50),
  DateDebut DATE,
  DateFin DATE NOT NULL,
  PRIMARY KEY (idOR)
) ENGINE InnoDB;


CREATE TABLE Etablissement (
  idET INT NOT NULL,
  NumEtud VARCHAR(50),
  Nom VARCHAR(50),
  Ville VARCHAR(50),
  Type VARCHAR(50),
  FraisSco FLOAT,
  Specialite VARCHAR(50),
  Niveau VARCHAR(50),
  DateDebut DATE,
  DateFin DATE,
  PRIMARY KEY (idET)
) ENGINE InnoDB;

CREATE TABLE Marque (
  idMA INT NOT NULL,
  Type VARCHAR(50),
  Localisation VARCHAR(50),
  Taille FLOAT,
  Origine VARCHAR(100),
  DateDebut DATE,
  DateFin DATE,
  PRIMARY KEY (idMA)
) ENGINE InnoDB;


CREATE TABLE Apparence (
  idAP INT NOT NULL,
  Taille FLOAT,
  Poids FLOAT,
  CouleurYeux VARCHAR(50),
  CouleurCheveux VARCHAR(50),
  LongueurCheveux VARCHAR(50),
  DateDebut DATE,
  DateFin DATE,
  PRIMARY KEY (idAP)
) ENGINE InnoDB;


CREATE TABLE Apparence_Marque (
  idMA INT NOT NULL,
  idAP INT NOT NULL,
  PRIMARY KEY (idMA, idAP)
) ENGINE InnoDB;


CREATE TABLE Banque (
  idBA INT NOT NULL,
  Nom VARCHAR(50),
  DateDebut DATE,
  DateFin DATE,
  PRIMARY KEY (idBA)
) ENGINE InnoDB;


CREATE TABLE Banque_Emprunt (
  idBA INT NOT NULL,
  idEM INT NOT NULL,
  PRIMARY KEY (idBA, idEM)
) ENGINE InnoDB;


CREATE TABLE CompteBancaire (
  idCM INT NOT NULL,
  NumCompte DOUBLE PRECISION,
  TypeCompte VARCHAR(50),
  DateDebut DATE,
  DateFin DATE,
  idBA INT NOT NULL,
  PRIMARY KEY (idCM)
) ENGINE InnoDB;


CREATE TABLE Adresse (
  idAD INT NOT NULL,
  Type VARCHAR(50),
  ComplementAdresse VARCHAR(50),
  Numero INT,
  TypeVoie VARCHAR(25),
  Voie VARCHAR(50),
  Pays VARCHAR(50),
  Ville VARCHAR(50),
  CodePostal VARCHAR(10),
  DateDebut DATE,
  DateFin DATE,
  PRIMARY KEY (idAD)
) ENGINE InnoDB;


CREATE TABLE Relations (
  idRE INT NOT NULL,
  Type VARCHAR(50),
  Identifiant INT,
  PRIMARY KEY (idRE)
) ENGINE InnoDB;


CREATE TABLE Localisation (
  idLO INT NOT NULL,
  Latitude FLOAT,
  Longitude FLOAT,
  TypeLieu VARCHAR(50),
  Date DATE,
  Duree FLOAT,
  PRIMARY KEY (idLO)
) ENGINE InnoDB;


CREATE TABLE Maladies (
  idML INT NOT NULL,
  Nom VARCHAR(50),
  Type VARCHAR(50),
  Stade VARCHAR(50),
  DateDebut DATE,
  DateFin DATE,
  PRIMARY KEY (idML)
) ENGINE InnoDB;


CREATE TABLE Poste (
  idPO INT NOT NULL,
  Poste VARCHAR(50),
  Salaire FLOAT,
  DateDebut DATE,
  DateFin DATE,
  PRIMARY KEY (idPO)
) ENGINE InnoDB;


CREATE TABLE Organisation_Poste (
  idPO INT NOT NULL,
  idOR INT NOT NULL,
  PRIMARY KEY (idPO, idOR)
) ENGINE InnoDB;


CREATE TABLE Interets (
  idIN INT NOT NULL,
  Type VARCHAR(50),
  Nom VARCHAR(50),
  Niveau VARCHAR(50),
  Divers VARCHAR(50),
  DateDebut DATE,
  DateFin DATE,
  PRIMARY KEY (idIN)
) ENGINE InnoDB;


CREATE TABLE Interets_Club (
  idCL INT NOT NULL,
  idIN INT NOT NULL,
  PRIMARY KEY (idCL, idIN)
) ENGINE InnoDB;


CREATE TABLE EtatCivil (
  idEC INT NOT NULL,
  LieuNaissance VARCHAR(50),
  DateNaissance DATE,
  PRIMARY KEY (idEC)
) ENGINE InnoDB;

CREATE TABLE EtatCivil_var (
  idECV INT NOT NULL,
  Nom VARCHAR(50),
  Nom_usage VARCHAR(50),
  Prenoms VARCHAR(50),
  Nationalites VARCHAR(100),
  Sexe CHAR,
  DateDebut DATE,
  DateFin DATE,
  idEC INT NOT NULL,
  PRIMARY KEY (idECV)
) ENGINE InnoDB;

CREATE TABLE Possesion (
  idPO INT NOT NULL,
  Type VARCHAR(50),
  Marque VARCHAR(50),
  Modele VARCHAR(50),
  TypeReference VARCHAR(50),
  Reference VARCHAR(50),
  Divers VARCHAR(100),
  DateDebut DATE,
  DateFin DATE,
  idEC INT NOT NULL,
  PRIMARY KEY (idPO)
) ENGINE InnoDB;


CREATE TABLE EtatCivil_Interets (
  idIN INT NOT NULL,
  idEC INT NOT NULL,
  PRIMARY KEY (idIN, idEC)
) ENGINE InnoDB;


CREATE TABLE EtatCivil_FAI (
  idFA INT NOT NULL,
  idEC INT NOT NULL,
  PRIMARY KEY (idFA, idEC)
) ENGINE InnoDB;


CREATE TABLE EtatCivil_HistoAchats (
  idEC INT NOT NULL,
  idHA INT NOT NULL,
  PRIMARY KEY (idEC, idHA)
) ENGINE InnoDB;


CREATE TABLE DonneesSante (
  idDS INT NOT NULL,
  idEC INT NOT NULL,
  NumSecu INT,
  GroupeSang VARCHAR(50),
  PRIMARY KEY (idDS)
) ENGINE InnoDB;


CREATE TABLE DonneesSante_Maladie (
  idDS INT NOT NULL,
  idML INT NOT NULL,
  PRIMARY KEY (idDS, idML)
) ENGINE InnoDB;


CREATE TABLE EtatCivil_Localisation (
  idEC INT NOT NULL,
  idLO INT NOT NULL,
  PRIMARY KEY (idEC, idLO)
) ENGINE InnoDB;


CREATE TABLE EtatCivil_Organisation (
  idEC INT NOT NULL,
  idOR INT NOT NULL,
  PRIMARY KEY (idEC, idOR)
) ENGINE InnoDB;


CREATE TABLE Education (
  idED INT NOT NULL,
  INE VARCHAR(50),
  Niveau VARCHAR(50),
  idEC INT NOT NULL,
  PRIMARY KEY (idED)
) ENGINE InnoDB;


CREATE TABLE Education_Certification (
  idCE INT NOT NULL,
  idED INT NOT NULL,
  PRIMARY KEY (idCE, idED)
) ENGINE InnoDB;


CREATE TABLE Education_Etablissement (
  idED INT NOT NULL,
  idET INT NOT NULL,
  PRIMARY KEY (idED, idET)
) ENGINE InnoDB;


CREATE TABLE EtatCivil_Banque (
  idEC INT NOT NULL,
  idBA INT NOT NULL,
  PRIMARY KEY (idEC, idBA)
) ENGINE InnoDB;


CREATE TABLE EtatCivil_Apparence (
  idEC INT NOT NULL,
  idAP INT NOT NULL,
  PRIMARY KEY (idEC, idAP)
) ENGINE InnoDB;


CREATE TABLE EtatCivil_Relations (
  idEC INT NOT NULL,
  idRE INT NOT NULL,
  PRIMARY KEY (idEC, idRE)
) ENGINE InnoDB;


CREATE TABLE Biometrie (
  idBI INT NOT NULL,
  idEC INT NOT NULL,
  ADN VARCHAR(150),
  FormeOreille VARCHAR(150),
  ReconnaissanceFaciale VARCHAR(150),
  ReconnaissanceIris VARCHAR(150),
  ReconnaissanceRetine VARCHAR(150),
  EmpreintesDigitales VARCHAR(150),
  ReconnaissanceMain VARCHAR(50),
  Odeur VARCHAR(150),
  ReconnaissanceVocale VARCHAR(150),
  ReconnaissanceVeine VARCHAR(50),
  PRIMARY KEY (idBI)
) ENGINE InnoDB;


CREATE TABLE Comptes (
  idCO INT NOT NULL,
  idEC INT NOT NULL,
  NomService VARCHAR(50),
  TypeService VARCHAR(50),
  URL VARCHAR(50),
  Identifiant VARCHAR(50),
  Password VARCHAR(50),
  Pseudo VARCHAR(50),
  AdresseEmail VARCHAR(50),
  DateDebut DATE,
  DateFin DATE,
  PRIMARY KEY (idCO)
) ENGINE InnoDB;


CREATE TABLE EtatCivil_Adresse (
  idEC INT NOT NULL,
  idAD INT NOT NULL,
  PRIMARY KEY (idEC, idAD)
) ENGINE InnoDB;


ALTER TABLE Interets_Club ADD CONSTRAINT club_interets_club_fk
FOREIGN KEY (idCL)
REFERENCES Club (idCL)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE EtatCivil_FAI ADD CONSTRAINT fai_etatcivil_fai_fk
FOREIGN KEY (idFA)
REFERENCES FAI (idFA)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE Contrat ADD CONSTRAINT fai_contrat_fk
FOREIGN KEY (idFA)
REFERENCES FAI (idFA)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE EtatCivil_HistoAchats ADD CONSTRAINT histoachats_etatcivil_histoachats_fk
FOREIGN KEY (idHA)
REFERENCES HistoAchats (idHA)
ON DELETE NO ACTION
ON UPDATE NO ACTION;


ALTER TABLE Banque_Emprunt ADD CONSTRAINT emprunt_banque_emprunt_fk
FOREIGN KEY (idEM)
REFERENCES Emprunt (idEM)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE Education_Certification ADD CONSTRAINT certification_education_certification_fk
FOREIGN KEY (idCE)
REFERENCES Certification (idCE)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE EtatCivil_Organisation ADD CONSTRAINT organisation_etatcivil_organisation_fk
FOREIGN KEY (idOR)
REFERENCES Organisation (idOR)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE Organisation_Poste ADD CONSTRAINT organisation_organisation_poste_fk
FOREIGN KEY (idOR)
REFERENCES Organisation (idOR)
ON DELETE NO ACTION
ON UPDATE NO ACTION;


ALTER TABLE Education_Etablissement ADD CONSTRAINT etablissement_education_etablissement_fk
FOREIGN KEY (idET)
REFERENCES Etablissement (idET)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE Apparence_Marque ADD CONSTRAINT marque_apparence_marque_fk
FOREIGN KEY (idMA)
REFERENCES Marque (idMA)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE EtatCivil_Apparence ADD CONSTRAINT apparence_etatcivil_apparence_fk
FOREIGN KEY (idAP)
REFERENCES Apparence (idAP)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE Apparence_Marque ADD CONSTRAINT apparence_apparence_marque_fk
FOREIGN KEY (idAP)
REFERENCES Apparence (idAP)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE EtatCivil_Banque ADD CONSTRAINT banque_etatcivil_banque_fk
FOREIGN KEY (idBA)
REFERENCES Banque (idBA)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE CompteBancaire ADD CONSTRAINT banque_comptebancaire_fk
FOREIGN KEY (idBA)
REFERENCES Banque (idBA)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE Banque_Emprunt ADD CONSTRAINT banque_banque_emprunt_fk
FOREIGN KEY (idBA)
REFERENCES Banque (idBA)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE EtatCivil_Adresse ADD CONSTRAINT adresse_etatcivil_adresse_fk
FOREIGN KEY (idAD)
REFERENCES Adresse (idAD)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE EtatCivil_Relations ADD CONSTRAINT relations_etatcivil_relations_fk
FOREIGN KEY (idRE)
REFERENCES Relations (idRE)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE EtatCivil_Localisation ADD CONSTRAINT localisation_etatcivil_localisation_fk
FOREIGN KEY (idLO)
REFERENCES Localisation (idLO)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE DonneesSante_Maladie ADD CONSTRAINT maladies_DonneesSante_Maladie_fk
FOREIGN KEY (idML)
REFERENCES Maladies (idML)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE Organisation_Poste ADD CONSTRAINT poste_organisation_poste_fk
FOREIGN KEY (idPO)
REFERENCES Poste (idPO)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE EtatCivil_Interets ADD CONSTRAINT interets_etatcivil_interets_fk
FOREIGN KEY (idIN)
REFERENCES Interets (idIN)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE Interets_Club ADD CONSTRAINT interets_interets_club_fk
FOREIGN KEY (idIN)
REFERENCES Interets (idIN)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE EtatCivil_Adresse ADD CONSTRAINT etatcivil_etatcivil_adresse_fk
FOREIGN KEY (idEC)
REFERENCES EtatCivil (idEC)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE Comptes ADD CONSTRAINT etatcivil_comptes_fk
FOREIGN KEY (idEC)
REFERENCES EtatCivil (idEC)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE Biometrie ADD CONSTRAINT etatcivil_biometrie_fk
FOREIGN KEY (idEC)
REFERENCES EtatCivil (idEC)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE EtatCivil_Relations ADD CONSTRAINT etatcivil_etatcivil_relations_fk
FOREIGN KEY (idEC)
REFERENCES EtatCivil (idEC)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE EtatCivil_Apparence ADD CONSTRAINT etatcivil_etatcivil_apparence_fk
FOREIGN KEY (idEC)
REFERENCES EtatCivil (idEC)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE EtatCivil_Banque ADD CONSTRAINT etatcivil_etatcivil_banque_fk
FOREIGN KEY (idEC)
REFERENCES EtatCivil (idEC)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE Education ADD CONSTRAINT etatcivil_education_fk
FOREIGN KEY (idEC)
REFERENCES EtatCivil (idEC)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE EtatCivil_Organisation ADD CONSTRAINT etatcivil_etatcivil_organisation_fk
FOREIGN KEY (idEC)
REFERENCES EtatCivil (idEC)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE EtatCivil_Localisation ADD CONSTRAINT etatcivil_etatcivil_localisation_fk
FOREIGN KEY (idEC)
REFERENCES EtatCivil (idEC)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE DonneesSante ADD CONSTRAINT etatcivil_donneessante_fk
FOREIGN KEY (idEC)
REFERENCES EtatCivil (idEC)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE EtatCivil_HistoAchats ADD CONSTRAINT etatcivil_etatcivil_histoachats_fk
FOREIGN KEY (idEC)
REFERENCES EtatCivil (idEC)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE EtatCivil_FAI ADD CONSTRAINT etatcivil_etatcivil_fai_fk
FOREIGN KEY (idEC)
REFERENCES EtatCivil (idEC)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE EtatCivil_Interets ADD CONSTRAINT etatcivil_etatcivil_interets_fk
FOREIGN KEY (idEC)
REFERENCES EtatCivil (idEC)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE EtatCivil_var ADD CONSTRAINT etatcivil_etatcivil_var_fk
FOREIGN KEY (idEC)
REFERENCES EtatCivil (idEC)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE Possesion ADD CONSTRAINT etatcivil_possesion_fk
FOREIGN KEY (idEC)
REFERENCES EtatCivil (idEC)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE DonneesSante_Maladie ADD CONSTRAINT donneessante_DonneesSante_Maladie_fk
FOREIGN KEY (idDS)
REFERENCES DonneesSante (idDS)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE Education_Etablissement ADD CONSTRAINT education_education_etablissement_fk
FOREIGN KEY (idED)
REFERENCES Education (idED)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE Education_Certification ADD CONSTRAINT education_education_certification_fk
FOREIGN KEY (idED)
REFERENCES Education (idED)
ON DELETE NO ACTION
ON UPDATE NO ACTION;
