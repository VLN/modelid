-- Sélection de la base de données
USE ModelId;

-- Insersion de données de tests dans la base de données ModelId
INSERT INTO EtatCivil(idEC, LieuNaissance, DateNaissance)
VALUES
  (1, 'Moulinsart', '1968-01-09'),
  (2, 'Bruxelles', '1969-01-09');

INSERT INTO EtatCivil_var(idECV, Nom, Nom_usage, Prenoms, Nationalites, Sexe, idEC)
VALUES
  (1, 'Haddock', '', 'Capitaine', 'Belge', 'M', 1),
  (2, 'Typhon', '', 'Tournesol', 'Belge, Française', 'M', 2);

INSERT INTO Adresse(idAD, Type, ComplementAdresse, Numero, TypeVoie, Voie, Pays, Ville, CodePostal, DateDebut, DateFin)
VALUES
  (1, 'Principale', '', 1, 'rue', 'Moulinsart', 'Belgique', 'Bruxelles', '', '1978-01-09', '0000-00-00'),
  (2, 'Principale', 'Apparement 26B', 27, 'rue', 'Salsberg', 'Belgique', 'Bruxelles', '', '2010-11-09', '0000-00-00'),
  (3, 'Étudiant', 'Apppartement 178', 5, 'avenue', 'du Roy', 'Belgique', 'Bruxelles', '', '1999-10-09', '2010-10-09');

  INSERT INTO EtatCivil_Adresse(idEC, idAD)
  VALUES
    (1, 1),
    (2, 2),
    (2, 3);

INSERT INTO FAI(idFA, Nom)
VALUES
  (1, 'Castafiore & Co'),
  (2, 'Kruger');

INSERT INTO Contrat(idCN, idFA, Type, Services, Reference, Tarif, Numero, Divers, DateDebut, DateFin)
VALUES
  (1, 1, 'Téléphonie', 'Fixe', 'AZ45658HGFIG-2', 12.29, '98-56-23-45-98', 'Domicilié adresse idAD=1', '1990-01-09', '0000-00-00'),
  (2, 2, 'Téléphonie', 'Mobile', '67GHJ6EDC7', 25.99, '06-56-23-45-98', 'illimité appels + SMS 20 Go', '2010-08-09', '0000-00-00'),
  (3, 1, 'Box internet', 'Télévision internet', 'FYEGHFZDVSHV', 40.99, '', 'fibre', '2007-08-09', '0000-00-00'),
  (4, 1, 'Box internet', 'Télévision internet téléphonie fixe', 'BYIGE-6-GBU', 55.99, '67-39-54-00-78', 'Domicilié adresse idAD=2', '2017-08-09','0000-00-00'),
  (5, 1, 'Téléphonie', 'Mobile', 'FZGFYRTCVY-4', 30.99, '06-89-45-22-94', 'illimité appels + SMS 40 Go', '2015-09-12', '0000-00-00');

INSERT INTO EtatCivil_FAI(IdFA, idEC)
VALUES
  (1, 1),
  (2, 1),
  (1, 2);

INSERT INTO Localisation(idLO, Latitude, Longitude, TypeLieu, Date, Duree)
VALUES
  (1, 50.850340, 4.351710, 'Bruxelles : centre-ville', '2019-08-11', 24),
  (2, 48.856763, 2.351739, 'Hôtel de Ville, Paris', '2019-05-12', 4),
  (3, 50.855197, 4.361358, 'Bruxelles : parc botanique', '2019-06-12', 2),
  (4, 51.503677, -0.133381, 'Londres : parc St James', '2019-04-12', 3),
  (5, 50.850340, 4.351710, 'Bruxelles : centre-ville', '2019-06-11', 24),
  (6, 40.712165, -74.001589, 'New-York : station de police', '2019-03-11', 48),
  (7, 40.712165, -74.001589, 'New-York : station de police', '2019-03-11', 24),
  (8, 50.850340, 4.351710, 'Bruxelles : centre-ville', '2019-06-11', 24),
  (9, 51.503677, -0.133381, 'Londres : parc St James', '2019-04-12', 3),
  (10, 48.856763, 2.351739, 'Hôtel de Ville, Paris', '2019-05-12', 4);

INSERT INTO EtatCivil_Localisation(idEC, idLO)
VALUES
  (1, 1),
  (1, 2),
  (1, 3),
  (1, 4),
  (1, 5),
  (1, 6),
  (1, 7),
  (2, 8),
  (2, 9),
  (2, 10);

INSERT INTO Club(idCL, Nom, Ville, DateDebut, DateFin)
VALUES
  (1, 'Alcooliques Anonymes', 'Bruxelles', '1999-05-12', '0000-00-00'),
  (2, 'Club des Amateurs de Whisky', 'Bruxelles', '1980-05-12', '0000-00-00'),
  (3, 'Les scientifques incompris', 'Bruxelles', '1982-07-10', '0000-00-00');

INSERT INTO Interets(idIN, Type, Nom, Niveau, Divers, DateDebut, DateFin)
VALUES
  (1, 'Boissons', 'Whisky', 'Grand-maître', 'consommation importante', '1975-03-05', '0000-00-00'),
  (2, 'Boissons', 'Vins français', 'Adepte', 'faible consommation', '1976-03-05', '0000-00-00'),
  (3, 'Boissons', 'Vins belges', 'Adepte', 'faible consommation', '1977-03-05', '1979-12-05'),
  (4, 'Scientifque', 'Inventions scientifiques', 'Génie', 'inventions loufoques', '1990-03-05', '0000-00-00'),
  (5, 'Scientifque', 'Voyage lunaire', 'Génie', 'voyage effectué', '1999-05-12', '0000-00-00');

INSERT INTO Interets_Club(idCL, idIN)
VALUES
  (1, 1),
  (2, 1),
  (3, 4);

INSERT INTO EtatCivil_Interets(idIN, idEC)
VALUES
  (1, 1),
  (2, 1),
  (3, 1),
  (4, 2),
  (5, 2);

INSERT INTO Possesion(idPO, Type, Marque, Modele, TypeReference, Reference, Divers, DateDebut, DateFin, idEC)
VALUES
  (1, 'Voiture', 'Citroën', 'DS', 'Immatriculation', '1-ABC-123', 'Verte', '1979-03-05', '0000-00-00', 1),
  (2, 'Pipe', 'Fumeurs & co', 'Pipe en bois', 'Aucune', '', '', '1999-05-12', '0000-00-00', 1),
  (3, 'Ordinateur portable', 'Dell', 'XPS 13', 'MAC', '5E:FF:56:A2:AF:15', 'Gris', '2017-05-12', '0000-00-00', 1),
  (4, 'Téléphonie portable', 'Apple', 'iPhone 10', 'MAC', '01:80:C2:00:00:01', 'Blanc numéro idCN=2','2016-05-12', '0000-00-00', 1),
  (5, 'Pendule', 'Invention personnelle', 'Standard', 'Aucune', '', 'A trouvé le trésor de la Licorne', '1977-03-05', '0000-00-00', 2),
  (6, 'Rollers à propulsion', 'Invention personnelle', 'Prototype 3', 'Aucune', '', 'Jaune à rayures noires, peu fiables', '2018-07-10', '0000-00-00', 2),
  (7, 'Rollers à propulsion', 'Invention personnelle', 'Prototype 2', 'Aucune', '', 'Verts, très peu fiables', '2017-07-10', '2017-10-10', 2),
  (8, 'Rollers à propulsion', 'Invention personnelle', 'Prototype 1', 'Aucune', '', 'Rouges, explosion', '2016-07-10', '2016-08-10', 2);

INSERT INTO HistoAchats(idHA, Moyen, Prix, Vendeur, Type, Ville, Nom, Reference, Marque, Date)
VALUES
  (1, 'Liquide', 250.99, 'Vins & co', 'Alcool', 'Bruxelles', 'Caisses de Whisky', '465h', 'Breizh Whisky', '2019-07-10'),
  (2, 'Carte Bancaire', 250.99, 'Vins & co', 'Alcool', 'Bruxelles', 'Caisses de Whisky', '465h', 'Breizh Whisky', '2019-08-10'),
  (3, 'Liquide', 250.99, 'Vins & co', 'Alcool', 'Bruxelles', 'Caisses de Whisky', '465h', 'Breizh Whisky', '2019-09-10'),
  (4, 'Liquide', 250.99, 'Vins & co', 'Alcool', 'Bruxelles', 'Caisses de Whisky', '465h', 'Breizh Whisky', '2019-10-10'),
  (5, 'Chèque', 250.99, 'Vins & co', 'Alcool', 'Bruxelles', 'Caisses de Whisky', '465h', 'Breizh Whisky', '2019-11-10'),
  (6, 'Liquide', 250.99, 'Vins & co', 'Alcool', 'Bruxelles', 'Caisses de Whisky', '465h', 'Breizh Whisky', '2019-12-10'),
  (7, 'Carte Bancaire', 45.98, 'Bureau de Tabac Central', 'Tabac', 'Bruxelles', 'Tabac à fumer', '57FGFYI7', 'Camel', '2019-10-09'),
  (8, 'Carte Bancaire', 22.30, 'Pendules & co', 'Divers', 'Bruxelles', 'Corde pour pendule', '576FH-2', 'Swhudg', '2016-08-10');

INSERT INTO EtatCivil_HistoAchats(idEC, idHA)
VALUES
  (1, 1),
  (1, 2),
  (1, 3),
  (1, 4),
  (1, 5),
  (1, 6),
  (1, 7),
  (2, 8);

INSERT INTO Comptes(idCO, idEC, NomService, TypeService, URL, Identifiant, Password, Pseudo, AdresseEmail, DateDebut, DateFin)
VALUES
  (1, 1, 'Instagram', 'Réseau social', 'https://www.instagram.com', 'CaptainHaddock', 'bachibouzouque23', 'Capitaine', 'capitaine-haddock@belgium-mail.be', '2016-11-10', '2017-11-10'),
  (2, 1, 'Forum des amateurs de whisky', 'Forum', 'https://forum-whisky.be', 'CaptainHaddock', 'bachibouzouque23', 'Capitaine', 'capitaine-haddock@belgium-mail.be', '1999-05-12', '0000-00-00'),
  (3, 2, 'Forum des scientifques incompris', 'Forum', 'https://forum-si.be', 'Prof-T', 'gufkdjb56d', 'Prof-T', 'professeur.touresol@belgium-mail.be', '1975-03-05', '0000-00-00'),
  (4, 2, 'Grande bibliothèque de Bruxelles', 'Bibliothèque', 'https://biblio.brussels', 'Typhon Tournesol', '', 'Typhon Tournesol', 'professeur.touresol@belgium-mail.be', '1977-03-05', '0000-00-00');

INSERT INTO Maladies(idML, Nom, Type, Stade, DateDebut, DateFin)
VALUES
  (1, "Alcoolisme", 'Addiction', 'Avancé', '1990-01-09', '0000-00-00');

INSERT INTO DonneesSante(idDS, idEC, NumSecu, GroupeSang)
VALUES
  (1, 1, '1347367', 'AB+'),
  (2, 2, '1567387', 'O-');

INSERT INTO DonneesSante_Maladie(idDS, idML)
VALUES
  (1, 1);

INSERT INTO Marque(idMA, Type, Localisation, Taille, Origine, DateDebut, DateFin)
VALUES
  (1, 'Grain beauté', 'bras gauche', 0.5, 'de naissance', '1968-01-09', '0000-00-00'),
  (2, 'Tâche de vin', 'cou', 1.1, 'de naissance', '1968-01-09', '0000-00-00'),
  (3, 'Brûlure', 'mains', 5, 'Accident de roller', '2016-07-10', '2016-10-10'),
  (4, 'Brûlure', 'mains', 5, 'Accident de roller','2017-07-10', '2017-10-10'),
  (5, 'Brûlure', 'mains', 5, 'Accident de roller','2018-07-10', '2018-10-10');

INSERT INTO Apparence(idAP, Taille, Poids, CouleurYeux, CouleurCheveux, LongueurCheveux, DateDebut, DateFin)
VALUES
  (1, 1.80, 82.5, 'Noir', 'Noir', 'Courts', '1990-01-09' , '0000-00-00'),
  (2, 1.75, 75.6, 'Noir', 'Noir', 'Calvicie', '1990-01-09' , '0000-00-00');

INSERT INTO EtatCivil_Apparence(idEC, idAP)
VALUES
  (1, 1),
  (2, 2);

INSERT INTO Apparence_Marque(idMA, idAP)
VALUES
  (1, 1),
  (2, 1),
  (3, 2),
  (4, 2),
  (5, 2);

INSERT INTO Biometrie(idBI, idEC, ADN, FormeOreille, ReconnaissanceFaciale, ReconnaissanceIris, ReconnaissanceRetine, EmpreintesDigitales, ReconnaissanceMain, Odeur, ReconnaissanceVocale, ReconnaissanceVeine)
VALUES
  (1, 1, 'filepath/file', 'filepath/file', 'filepath/file', 'filepath/file', 'filepath/file', 'filepath/file', 'filepath/file', 'filepath/file', 'filepath/file', 'filepath/file'),
  (2, 2, 'filepath/file', 'filepath/file', 'filepath/file', 'filepath/file', 'filepath/file', 'filepath/file', 'filepath/file', 'filepath/file', 'filepath/file', 'filepath/file');

INSERT INTO Poste(idPO, Poste, Salaire, DateDebut, DateFin)
VALUES
  (1, 'Rentier', 5500.99, '1998-01-09', '0000-00-00'),
  (2, 'Inventeur junior', 2000.00, '2000-02-10', '2010-06-11'),
  (3, 'Inventeur', 2500.00, '2010-06-11', '0000-00-00'),
  (4, 'PDG', 0.00, '2010-06-11', '0000-00-00');

INSERT INTO Organisation(idOR, Nom, Type, SIREN, Secteur, DateDebut, DateFin)
VALUES
  (1, 'Haddock & co', 'Entreprise privée', '3246575', 'Holding', '1998-01-09', '0000-00-00'),
  (2, 'Marivik Int.', 'Entreprise privée', '6868657', 'Recherche et Développement', '2000-02-10', '2010-06-11'),
  (3, 'Les Inventions de Tournesol', 'Entreprise privée', '67898697', 'Inventions scientifiques', '2010-06-11', '0000-00-00');

INSERT INTO Organisation_Poste(idPO, idOR)
VALUES
  (1, 1),
  (2, 2),
  (3, 3),
  (4, 3);

INSERT INTO EtatCivil_Organisation(idEC, idOR)
VALUES
  (1, 1),
  (2, 2),
  (2, 3);

INSERT INTO Certification(idCE, Nom, Domaine, Organisme, Reference, DateDebut, Datefin)
VALUES
  (1, 'Inventeur de génie', 'Invention', 'Cercle des inventeurs incompris', '678-VG-45', '2000-02-10', '0000-00-00');

INSERT INTO Education(idED, idEC, INE, Niveau)
VALUES
  (1, 1, '4567VTFYGH', 'BAC'),
  (2, 2, '576896GGFR', 'Doctorant');

INSERT INTO Education_Certification(idCE, idED)
VALUES
  (1, 2);

INSERT INTO Etablissement(idET, NumEtud, Nom, Ville, Type, FraisSco, Specialite, Niveau, DateDebut, DateFin)
VALUES
  (1, '34567565', 'Université de Bruxelles', 'Bruxelles', 'Faculté', 225.32, 'Physique appliquée', 'Licence', '1995-01-09', '1998-01-09'),
  (2, '345799844', 'Université de Liège', 'Liège', 'Faculté', 225.32, 'Physique & mathématiques', 'Master', '1998-01-09', '1999-01-09'),
  (3, '34567565', 'Université de Bruxelles', 'Bruxelles', 'Faculté', 225.32, 'Vie marine', 'Doctorat', '2000-02-10', '2003-02-10');

INSERT INTO Education_Etablissement(idED, idET)
VALUES
  (2, 1),
  (2, 2),
  (2, 3);

INSERT INTO Banque(idBA, Nom, DateDebut)
VALUES
  (1, 'Banque Nationale Belge', '1998-01-09'),
  (2, 'Crédit Mutuel Belge', '1998-01-09');

INSERT INTO CompteBancaire(idCM, idBA, NumCompte, TypeCompte, DateDebut)
VALUES
  (1, 2, '456567', 'Courant', '1998-01-09'),
  (2, 1, '23456521', 'Courant', '1998-01-09');

INSERT INTO EtatCivil_Banque(idEC, idBA)
VALUES
  (1, 1),
  (2, 2);

INSERT INTO Emprunt(idEM, Montant, TauxInteret, TauxAssurance, Duree, DateDebut, DateFin)
VALUES
  (1, 250000, 2.10, 15.5, 1.0, '2010-06-11', '2026-06-11');

INSERT INTO Banque_Emprunt(idBA, idEM)
VALUES
  (2, 1);

INSERT INTO Relations(idRE, Type, Identifiant)
VALUES
  (1, 'Amis', 2),
  (2, 'Amis', 1);

INSERT INTO EtatCivil_Relations(idEC, idRE)
VALUES
  (1, 1),
  (2, 2);
